(function ($) {
    
      $('#btnLoadText').click(function () { $("#showResult").load("show.txt"); });
      $('#btnAjax').click(function () { callRestAPI() });
    
      // Perform an asynchronous HTTP (Ajax) API request.
      function callRestAPI() {
        var root = 'https://reqres.in/api/users/2';
        $.ajax({
          url: root,
          method: 'GET'
        }).then(function (response) {

          $('#showResult').html('<h1>'+response.data.first_name+','+response.data.last_name+'</h1>'+'<img src="'+response.data.avatar+'"> </img>')
        });
      }
    })($);
    